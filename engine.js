const puppeteer = require('puppeteer')
const Promise = require('bluebird')
const axios = require('axios')
const _ = require('lodash')
const moment = require('moment')
const os = require('os')

const HEADLESS = (process.env['HEADLESS'] || 'true') === 'true'
const NO_SANDBOX = (process.env['NO_SANDBOX'] || 'true') === 'true'
const authToken = process.env['TOKEN']

const AUTH_TOKEN = `Token ${authToken}`
axios.defaults.headers.common['Authorization'] = AUTH_TOKEN

const WORKER_NAME = `${process.env['USER'] || 'any'}@${os.hostname()}`

const RUNNING_TASKS = {}
const MAX_TASKS = 10
const MAX_TASKS_BY_CRAWLER = 1
const IMPLODE_TIME = 1000 * 60 * 30

const HEADERS = {
  useragent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3347.0 Safari/537.36',
  extra: {
    'accept-language': 'en-US,en;q=0.8',
  },
}

const HOST = process.env['HOST'] || 'http://localhost:8000'

const baseUrl = `${HOST}/api/v1`
const tasksUrl = `${baseUrl}/tasks/`

function newTaskHelper (crawler) {
  return async function (method, ...args) {
    return axios.post(tasksUrl, {
      crawler,
      method,
      args: [...args],
    })
  }
}

async function deleteRemote (url) {
  try {
    await axios.delete(url)
  } catch (e) {
    if (e.response && e.response.status === 404) {
      console.log('Task is already deleted why?', url)
    } else {
      console.error('Problem deleting the task', e.response)
    }
  }
}

async function sendTaskError (task, error) {
  let settings = {headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}}
  let data = JSON.stringify({ error: error })
  try {
    await axios.patch(task.url, data, settings)
  } catch (e) {
    console.log('Problem sending error:', e)
  }
}

async function checkCompleted () {
  const promises = _.map(RUNNING_TASKS, async (task, assignTask) => {
    const promise = task.promise
    const availPages = task.crawlerModule._pages

    if (promise.isFulfilled()) {
      clearInterval(task.assignBeatInterval)
      availPages.push(task.page)
      await deleteRemote(task.assignUrl)
      await deleteRemote(task.url)
      delete RUNNING_TASKS[task.id]
      console.log('Task is done:', task.crawler, task.method, 'total', moment().diff(task.startTime, 'minutes'))
      return
    }

    if (promise.isRejected() || promise.isCancelled()) {
      console.log('Task has failed', task.crawler, task.method, task.task)
      clearInterval(task.assignBeatInterval)
      availPages.push(task.page)
      await deleteRemote(task.assignUrl)
      delete RUNNING_TASKS[task.id]
      promise.catch(TypeError, function (error) {
        console.log(error)
        sendTaskError(task, error)
      }).catch(ReferenceError, function (error) {
        console.log(error)
        sendTaskError(task, error)
      }).catch(function (error) {
        sendTaskError(task, error.response.status+' - '+error.response.statusText)
      })
    }
  })
  await Promise.all(promises)
}

async function initWorker () {
  const randName = Math.random().toString(36).substring(2)
  const workerName = `${WORKER_NAME}-${randName}`
  console.log('STARTING WORKER', workerName)
  await axios.post(`${baseUrl}/workers/`, { name: workerName })
  const workerUrl = `${baseUrl}/workers/${workerName}/`

  setInterval(async function () {
    try {
      await axios.patch(workerUrl)
    } catch (e) {
      if (e.response && e.response.status === 404) {
        console.log('worker was expired, but registering again')
        await axios.post(`${baseUrl}/workers/`, { name: workerName })
      } else {
        console.error('Failed to patch worker', e.response)
      }
    }
  }, 60000)

  // Closing process deletes remote worker
  process.on('SIGINT', async function () {
    console.log('shutting down worker')
    try {
      await axios.delete(workerUrl)
    } finally {
      process.exit()
    }
  })

  return workerUrl
}

function canGetNewTask () {
  return _.size(RUNNING_TASKS) < MAX_TASKS
}

async function newBrowser () {
  return puppeteer.launch({
    headless: HEADLESS,
    args: NO_SANDBOX ? ['--no-sandbox', '--disable-setuid-sandbox'] : [],
  })
}

async function dummyFunction () {
  console.log('dummy Function start and ends')
}

function setNoTaskTimeout (crawler) {
  crawler.__lastTime = new Date()
  if (crawler.__noTaskTimeout) {
    clearTimeout(crawler.__noTaskTimeout)
  }

  crawler.__noTaskTimeout = setTimeout(async function () {
    console.log('Closing', crawler._name, 'No more tasks since')
    if (crawler._close) {
      const page = await getPage(crawler)
      try {
        await crawler._close({ page })
      } catch (e) {
        console.error('Failed to close page', e)
      }
    }
    await crawler._implode()
  }, IMPLODE_TIME)
}

function getBrowser (crawler) {
  return crawler._browser
}

async function getPage (crawler) {
  return crawler._pages.pop() || crawler._browser.newPage()
}

async function setUpCrawler (crawler, name, requirePath) {
  if (crawler._name) return

  console.log(`===== Setting UP "${name}" =====`)
  crawler._name = name
  crawler._browser = await newBrowser()
  crawler._pages = []
  crawler._fullRequirePath = require.resolve(requirePath)
  crawler._implode = async function () {
    console.log(` ====== imploding "${crawler._name}" ======`)
    await crawler._browser.close()
    delete require.cache[crawler._fullRequirePath]
  }
}

async function main (crawlersPath = '../../') {
  const workerUrl = await initWorker()
  const assignsUrl = `${workerUrl}assigns/`
  const http = axios.create({
    baseURL: baseUrl,
  })

  http.defaults.headers.common['Authorization'] = AUTH_TOKEN
  while (true) {
    await checkCompleted()
    await Promise.delay(10000)

    if (!canGetNewTask()) continue

    let data = []
    try {
      ({ data } = await axios.get(assignsUrl))
    } catch (e) {
      console.error('server or connection is failing')
      continue
    }

    for (let task of data) {
      if (RUNNING_TASKS[task.id]) continue
      if (_.size(RUNNING_TASKS) >= MAX_TASKS) {
        break
      }

      const crawlerTasks = _.filter(RUNNING_TASKS, (t, id) => t.crawler === task.crawler)
      if (crawlerTasks.length >= MAX_TASKS_BY_CRAWLER) {
        continue
      }

      const assignUrl = `${assignsUrl}${task.id}/`
      const requirePath = `${crawlersPath}${task.crawler}`
      var crawler
      try {
        crawler = require(requirePath)
      } catch (e) {
        continue
      }

      console.log('Starting Task:', task.crawler, task.method)
      await setUpCrawler(crawler, task.crawler, requirePath)
      setNoTaskTimeout(crawler)
      const browser = getBrowser(crawler)
      const page = task.page = await getPage(crawler)
      page.setViewport({width: 1900, height: 1400})
      page.setUserAgent(HEADERS.useragent)
      page.setExtraHTTPHeaders(HEADERS.extra)
      const newTask = newTaskHelper(task.crawler)
      const args = _.isPlainObject(task.args) ? task.args : {}
      const arrayArgs = _.isArray(task.args) ? task.args : []
      let method = crawler[task.method]
      if (!method && task.method.startsWith('_')) {
        // Duymmy function to let the promise
        method = dummyFunction
      }

      method = method.bind(crawler)
      const promise = method({ browser, page, args: task.args, newTask, http, ...args }, ...arrayArgs)
      task.crawlerModule = crawler
      task.startTime = moment()
      task.assignBeatInterval = setInterval(async function () {
        console.log('Running task:', task.task, moment().diff(task.startTime, 'minutes'), 'min')
        try {
          await axios.patch(assignUrl)
        } catch (e) {
          if (e.response && e.response.status === 404) {
            console.log('assign was deleated')
            clearInterval(task.assignBeatInterval)
          } else {
            console.error('task beat assign failing', e.response)
          }
        }
      }, 60000)
      task.promise = Promise.all([promise])
      task.url = `${tasksUrl}${task.task}/`
      task.assignUrl = `${assignsUrl}${task.id}/`

      RUNNING_TASKS[task.id] = task
    }
  }
}

module.exports = {
  start: main,
}
